defmodule Toll do
  require Logger
  @type get_words_error :: {:error, String.t()}
  @type get_words_output :: {:ok, List}

  @doc """
  Enter your Number to get possible words from dictionary
  """

  @spec get_words(number | String.t()) :: get_words_error | get_words_output
  def(get_words(phone)) do
    number =
      case is_number(phone) do
        true ->
          Integer.to_string(phone)

        false ->
          phone
      end

    case(String.match?(number, ~r/1|2|3|4|5|6|7|8|9/)) do
      false ->
        {:error, "unexpected char found in input"}

      true ->
        {:ok, propable_words(number)}
    end
  end

  @spec propable_words(String.t()) :: get_words_output
  def propable_words(phone) do
    start = Timex.now()
    ConCache.get(:toll, phone)
    list_of_split_phone = phone_possible(phone)

    data =
      Enum.reduce(list_of_split_phone, [], fn x, acc ->
        {first, last} = x
        {first, last} = {ConCache.get(:toll, first), ConCache.get(:toll, last)}

        check =
          if first == nil || last == nil do
            false
          else
            true
          end

        case check do
          true ->
            items =
              Enum.map(first, fn x ->
                Enum.map(last, fn y ->
                  {x, y}
                end)
              end)

            [items | acc]

          false ->
            acc
        end
      end)
      |> propable_words_entire(phone)
      |> List.flatten()

    diff = Timex.diff(Timex.now(), start)
    Logger.warn("Time taken #{diff} microseconds")
    data
  end

  def propable_words_entire(payload, phone) do
    case ConCache.get(:toll, phone) do
      nil ->
        payload

      data ->
        [data | payload]
    end
  end

  @doc """
  Mini of 3 char required, so splitting phone number between 3 and 7
  """
  @spec phone_possible(String.t()) :: map
  def phone_possible(phone) do
    Enum.map(3..7, fn x -> String.split_at(phone, x) end)
  end

  @doc """
  Reading File and sending chunk into parse_list
  """
  @spec read_dict() :: :ok
  def read_dict do
    start = Timex.now()
    ConCache.start_link(name: :toll, ttl_check_interval: false)

    File.stream!("dictionary.txt")
    |> Stream.chunk_every(5)
    |> Enum.each(fn chunk ->
      parse_list(chunk)
    end)

    diff = Timex.diff(Timex.now(), start)
    Logger.warn("Time taken to insert into ets - #{diff / 1_000_000} - seconds")
  end

  @spec parse_list(List) :: :ok
  def parse_list(list) do
    Enum.each(list, fn item ->
      [special_char, word, indexed_word] = parse_item(item)

      case special_char do
        true ->
          :ok

        false ->
          ets_insert(indexed_word, word)
      end
    end)
  end

  def ets_insert(indexed_word, word) do
    case ConCache.get(:toll, indexed_word) do
      nil ->
        ConCache.put(:toll, indexed_word, [word])

      data ->
        ConCache.put(:toll, indexed_word, [word | data])
    end
  end

  def parse_item(item) do
    word =
      case String.split(item, "\n") do
        [word] ->
          word

        [word, _] ->
          word
      end

    list_of_char = create_indexed_number(word)

    indexed_word =
      Enum.map(list_of_char, fn item ->
        coding(item)
      end)
      |> Enum.join()

    check_special = String.match?(indexed_word, ~r/0/)
    [check_special, word, indexed_word]
  end

  @doc """
  Spliing the word into individual characters
  """
  @spec create_indexed_number(String.t()) :: list(String.t())
  def create_indexed_number(word) do
    String.split(word, "", trim: true)
  end

  @doc """
  Mapping the words into integers
  """
  @spec coding(String.t()) :: integer
  def coding(item) do
    case item do
      item when item in ["A", "B", "C"] ->
        2

      item when item in ["D", "E", "F"] ->
        3

      item when item in ["G", "H", "I"] ->
        4

      item when item in ["J", "K", "L"] ->
        5

      item when item in ["M", "N", "O"] ->
        6

      item when item in ["P", "Q", "R", "S"] ->
        7

      item when item in ["T", "U", "V"] ->
        8

      item when item in ["W", "X", "Y", "Z"] ->
        9

      _ ->
        Logger.warn("unknown char found #{item}")
        0
    end
  end
end
