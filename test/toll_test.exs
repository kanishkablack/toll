defmodule TollTest do
  use ExUnit.Case
  doctest Toll

  test "Coding Elements" do
    eval_a = Toll.coding("A")
    eval_d = Toll.coding("D")
    eval_g = Toll.coding("G")
    eval_j = Toll.coding("J")
    eval_m = Toll.coding("M")
    eval_p = Toll.coding("P")
    eval_t = Toll.coding("T")
    eval_w = Toll.coding("W")
    eval_special = Toll.coding("$")

    assert eval_a == 2
    assert eval_d == 3
    assert eval_g == 4

    assert eval_j == 5
    assert eval_m == 6
    assert eval_p == 7

    assert eval_t == 8
    assert eval_w == 9

    assert eval_special == 0
  end

  test "split word into individual char" do
    word = String.split("split", "", trim: true)
    assert word == ["s", "p", "l", "i", "t"]
  end

  test "testing pased item" do
    data = Toll.parse_item("ACT")
    assert data == [false, "ACT", "228"]
  end

  test "testing pased item with special char" do
    data = Toll.parse_item("ACT$")
    assert data == [true, "ACT$", "2280"]
  end

  test "input char" do
    {resp, __} = Toll.get_words("aasd")
    assert resp == :error
  end
end
