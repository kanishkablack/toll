# Toll

[![pipeline status](https://gitlab.com/kanishkablack/toll/badges/master/pipeline.svg)](https://gitlab.com/kanishkablack/toll/commits/master)

## Assumed

* words in the dictionary are capitals

## Getting Started

To avoid confusion, please rename the dictionary file to `dictionary.txt`

Upon staring application,

```
Toll.read_dict
```

Sample output - takes some time to insert, depends on your size
```
Toll.read_dict

09:23:21.104 [warn]  Time taken to insert into ets - 4.719322 - seconds
```

### Getting words for phone

```
Toll.get_words 6686787825

10:17:35.095 [warn]  Time taken 137 microseconds
{:ok,
 [
   "MOTORTRUCK",
   {"NOUNS", "USUAL"},
   {"NOUNS", "TRUCK"},
   {"MOTOR", "USUAL"},
   {"MOTOR", "TRUCK"},
   {"ONTO", "STRUCK"},
   {"NOUN", "STRUCK"}
 ]}

```

## Develop patterns

* First of all, I don't want to over engineer stuff, could implement in multiple way, like genserver to start and work on it.

* Railway Oriented programming, like {:ok, data} || {:error, _ }. I got inflused by https://fsharpforfunandprofit.com/rop/ and https://medium.com/elixirlabs/railway-oriented-programming-in-elixir-with-pattern-matching-on-function-level-and-pipelining-e53972cede98

* I believe Railway Oriented programming, will reduce run time errors and effective handling of errors. we can see this patterns in `Ecto` and `phoneix` started to implement in controller in 1.4 version.

*  Few cases where, few functions where validation of string is not taken care. I believe I that should be done at initial level, but not at when real code is executed.(I depends on application and strategy).

## Questions

```
2282668687 should return the following list
[
  ["act", "amounts"],
  ["act", "contour"],
  ["acta", "mounts"],
  ["bat", "amounts"],
  ["bat", "contour"],
  ["cat", "contour"],
  "catamounts"
]
```

```
iex(10)> Toll.get_words 2282668687

10:33:36.407 [warn]  Time taken 154 microseconds
{:ok,
 [
  {"ACTA", "MOUNTS"},
   "CATAMOUNTS",
   {"CAT", "CONTOUR"},
   {"CAT", "AMOUNTS"},
   {"BAT", "CONTOUR"},
   {"BAT", "AMOUNTS"},
   {"ACT", "CONTOUR"},
   {"ACT", "AMOUNTS"}
 ]}
```


`["cat", "amounts"]` is missing in sample you provided, is it any specific reason Or is it to test ?
